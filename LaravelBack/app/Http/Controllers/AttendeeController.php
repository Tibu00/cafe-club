<?php

namespace App\Http\Controllers;

use App\Models\Attendee;
use Illuminate\Http\Request;

class AttendeeController extends Controller
{
    public function index(Request $request)
    {
        $attendees = Attendee::with('event')->get();
        return response()->json(['data' => $attendees]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'phone_number' => 'required|string',
            'event_id' => 'required|exists:events,id'
        ]);

        $attendee = Attendee::create($validatedData);

        return response()->json(['data' => $attendee]);
    }

    public function show($id)
    {
        $attendee = Attendee::with('event')->findOrFail($id);
        return response()->json(['data' => $attendee]);
    }
}
