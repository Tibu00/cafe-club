# Café club

Cloner le repo git

## Installer toute les dépendances

Composer update dans votre fichier laravelBack
Faire npm i ou npm install dans votre fichier VueFront

## Configurer le .env

Configurer le fichier .env dans LaravelBack de cette manière:

recopier le .env.example dans un nouveau fichier que vous nommerez .env et changer les données ci-dessous:

DB_CONNECTION=mysql // votre systeme de gestion de base de donnée //
DB_HOST=127.0.0.1
DB_PORT=3306 // Votre database port //
DB_DATABASE=cafe // Votre nom pour la database que vous utilisez, créer la sur mysql de préférence en amont //
DB_USERNAME=root // Votre username pour acceder a votre database //
DB_PASSWORD= // Votre mot de passe pour la database //

Faire php artisan migrate dans LaravelBack pour migrer toute les tables et vérifier si celle-ci correspondent dans mysql/postgre etc...
Vous êtes censé avoir 4 tables nommées : attendees, events, migrations et personnal_access_token (que vous pouvez delete).

## Launch Serve

Faire la commande à partir de cafe-club :

chmod +x script.sh

Cela vous donnera la permission d'executer le script suivant :

./script.sh

## Votre projet est bon pour être utilisé désormais !
