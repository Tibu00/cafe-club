export function formatDate(dateString: string) {
  const date = new Date(dateString);
  const months: string[] = [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre",
  ];
  const day: string = [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
  ][date.getDay()];
  const month: string = months[date.getMonth()];
  const dayOfMonth: number = date.getDate();
  return `${day} ${dayOfMonth} ${month}`;
}
