export function goToAttendeeForm(
  this: any,
  eventId: number,
  eventName: string
) {
  this.$router.push({
    name: "event-attendee-form", // assuming you've defined a named route for the form view
    params: { eventId },
    query: { eventName },
  });
}
