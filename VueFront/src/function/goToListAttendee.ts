export function goToListAttendee(
  this: any,
  eventId: number,
  eventName: string
) {
  this.$router.push({
    name: "event-attendee-list",
    params: { eventId },
    query: { eventName },
  });
}
