export interface Event {
  id: number;
  name: string;
  date: Date;
  description: string;
  attendees_count: Number;
}
