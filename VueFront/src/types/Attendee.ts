export interface Attendee {
  id: number;
  name: string;
  phone_number: string;
  event_id: number;
}
