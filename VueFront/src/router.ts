import { createRouter, createWebHistory } from "vue-router";
import AttendeeForm from "./views/AttendeeForm.vue";
import ListEventsVue from "./views/ListEvents.vue";
import ListAttendeesVue from "./views/ListAttendees.vue";
import CreateEventFormVue from "./views/CreateEventForm.vue";

const routes = [
  {
    path: "/",
    name: "ListEvent",
    component: ListEventsVue,
  },
  {
    path: "/events/:eventId/attendees/new",
    name: "event-attendee-form",
    component: AttendeeForm,
    props: true,
  },
  {
    path: "/events/:eventId/attendees/list",
    name: "event-attendee-list",
    component: ListAttendeesVue,
    props: true,
  },
  {
    path: "/CreateEvent",
    name: "create-event",
    component: CreateEventFormVue,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
